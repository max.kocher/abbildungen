To create figures with these files, [Python](https://www.python.org/downloads/) and [matplotlib](https://matplotlib.org/stable/index.html) must be installed.

In the last lines you can use `plt.show()` to show the figure itself and [`plt.savefig()`](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.savefig.html) to save it as a file.
